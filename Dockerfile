FROM python:3.11-bullseye

RUN apt-get update && apt-get install -y git autoconf libtool swig python3-dev texinfo build-essential gcc

WORKDIR /libredwgcode
RUN git clone git://git.sv.gnu.org/libredwg.git /libredwgcode
RUN sh autogen.sh 
RUN ./configure --enable-trace # (this will enable debugging messages) 
RUN make 
RUN make install 
RUN make check

ENV LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH

WORKDIR /code

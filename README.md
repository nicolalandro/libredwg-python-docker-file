# Docker file for LibreDWG Python
This remo have a dockerfile configured for use LibreDWG with python.

## How to run

```
docker-compose run libredwg bash
python src/load_dwg.py data/sample.dwg
```

## References
* docker, docker compose
* python
* LibreDWG:
  * [lib code](https://github.com/LibreDWG/libredwg)
  * [gnu doc info](https://www.gnu.org/software/libredwg/)
  * [How to install](https://medium.com/@gaganjyot/libredwg-installation-on-linux-f31cf9a2fdff)
  * [python example](https://github.com/simple-deploy/LibreGWD-python-examples/tree/master)
* [drawing to text](https://github.com/davidworkman9/drawingtotext) a lib that use LibreDWG to extract text from dwg files